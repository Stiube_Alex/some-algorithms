﻿using System;

namespace Time_Conversion
{
    class Program
    {
        static void Main(string[] args)
        {
            string time = "12:05:45PM";
            Console.WriteLine(time);
            Console.WriteLine(workString(time));
        }
        static string workString(string s)
        {
            string[] test = { "HH" };
            if (s[8] == 'A' && s[1] == '2')
            {
                string trimAM = s.Remove(8);
                test = trimAM.Split(':');
                string specialAM = "00:" + test[1] + ":" + test[2];
                return specialAM;
            }
            if (s[8] == 'A' && s[1] != '2')
            {
                string typeAM = s.Remove(8);
                return typeAM;

            }
            if (s[8] == 'P' && s[1] == '2')
            {
                string trimPM = s.Remove(8);
                test = trimPM.Split(':');
                string typePM = "12:" + test[1] + ":" + test[2];
                return typePM;
            }
            if (s[8] == 'P' && s[1] != '2')
            {
                string trimPM = s.Remove(8);
                test = trimPM.Split(':');
                int result = Int32.Parse(test[0]);
                result += 12;
                string typePM = result + ":" + test[1] + ":" + test[2];
                return typePM;
            }
            if (s[8] == 'P')
            {
                string trimPM = s.Remove(8);
                test = trimPM.Split(':');
                string typePM = "14:" + test[1] + ":" + test[2];
                return typePM;
            }
            return "Format Error";
        }
    }
}
