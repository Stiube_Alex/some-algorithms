﻿using System;

namespace Reverse_Array
{
    class Program
    {
        static Random r = new Random();
        static void Main(string[] args)
        {
            int n = 5;
            int[] arr = new int[n];
            int[] reverse = new int[arr.Length];
            int first = 0;
            for (int i = 0; i < n; i++)
            {
                arr[i] = r.Next(10);
                first = arr[0];
                Console.Write(arr[i] + " ");
            }
            Console.WriteLine();

            int a = 0;
            for (int j = arr.Length - 1; j >= 0; j--)
            {
                reverse[a] = arr[j];
                a++;
            }

            for (int i = 0; i < n; i++)
            {
                Console.Write(reverse[i] + " ");
            }


            // Console.Write(first);
        }
    }
}
