﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Euclid_s_Algorithm
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.WriteLine("Enter 2 numbers:");
			int a = int.Parse(Console.ReadLine());
			int b = int.Parse(Console.ReadLine());
			int c;
			while (b != 0)
			{
				c = a % b;
				a = b;
				b = c;
			}
			Console.WriteLine("Result is: ");
			Console.WriteLine(a);
			Console.ReadKey();
		}
	}
}