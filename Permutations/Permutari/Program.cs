using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Permutari
{
    class Program
    {
        static bool valid(int m, int[] v, int val)
        {
            for (int i = 0; i < m; i++)
            {
                if (v[i] == val)
                {
                    return true;
                }
            }
            return false;
        }

        static void back(int n, int m, int[] v)
        {
            if (m == n)
            {
                print(n, v);
                Console.WriteLine();
                return;
            }
            for (int i = 0; i < n; i++)
            {
                if (!valid(m, v, i))
                {
                    v[m] = i;
                    back(n, m + 1, v);
                }
            }
        }

        static void permutare(int n, int[] v)
        {
            back(n, 0, v);
        }

        static void print(int n, int[] v)
        {
            for (int i = 0; i < n; i++)
            {
                Console.Write(v[i] + " ");
            }
        }

        static void Main(String[] nameargs)
        {
            int p = 3;
            int[] v = new int[p];
            permutare(p, v);
            Console.ReadKey();
        }
    }
}