# Explicatii:

**Programul are cinci funcții:**
1. `Main`
1. `print`
1. `permutare`
1. `back`
1. `ok`
 
---

În `Main` putem modifica variabila `p` care definește numărul până la care programul va genera permutări (începând de la 0).
Funcția `print` are rolul de a afișa rezultatul în consolă.
Funcția `permutare` ne redirecționează în `back` cu valori de început, `m = 0`.
După ce avem valorea de început funcția `back` începe să genereze permutări.
Aceasta, în cazul în care `ok` este `false` va aduna `m` cu 1 și va repeta acțiunea.

---

