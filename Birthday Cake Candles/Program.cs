﻿using System;
using System.Collections.Generic;

namespace Birthday_Cake_Candles
{
    class Program
    {
        static Random r = new Random();
        static void Main(string[] args)
        {
            List<int> candle = new List<int>();
            generateList(candle);
            examineList(candle);
        }
        static void generateList(List<int> candle)
        {
            int n = 4;
            for (int i = 0; i < n; i++)
            {
                candle.Add(r.Next(5));
            }
        }
        static void examineList(List<int> candle)
        {
            int max = 0;
            int count = 0;
            foreach (int key in candle)
            {
                Console.WriteLine(key);
                if (max < key)
                {
                    max = key;
                }
            }
            foreach (var key in candle)
            {
                if (max == key)
                {
                    count++;
                }
            }
            Console.WriteLine("\n" + max);
            Console.WriteLine(count);
        }
    }
}
