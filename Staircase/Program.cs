﻿using System;

namespace Staircase
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = 5; //int.Parse(Console.ReadLine());
            staircase(n);
        }
        static void staircase(int n)
        {
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if (i + j >= n - 1)
                    {
                        Console.Write("# ");
                    }
                    else
                    {
                        Console.Write("  ");
                    }
                }
                Console.WriteLine();
            }
        }
    }
}
