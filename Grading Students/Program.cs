﻿using System;
using System.Collections.Generic;

namespace Grading_Students
{
    class Program
    {
        static Random r = new Random();
        static void Main(string[] args)
        {
            List<int> grades = new List<int>(4);
            generateGrades(grades);
            print(grades);
            rounding(grades);
        }
        static void print(List<int> grades)
        {
            foreach (var item in grades)
            {
                Console.WriteLine(item);
            }
        }
        static List<int> generateGrades(List<int> grades)
        {
            for (int i = 0; i < grades.Capacity; i++)
            {
                grades.Add(r.Next(101));
            }
            return grades;
        }
        static void rounding(List<int> grades)
        {

            Console.WriteLine();
            int size;
            size = grades.Count;
            for (int i = 0; i < size; i++)
            {
                if (grades[i] > 37)
                {
                    if ((grades[i] + 1) % 5 == 0)
                    {
                        grades[i]++;
                    }
                    if ((grades[i] + 2) % 5 == 0)
                    {
                        grades[i] += 2;
                    }
                }
            }
            foreach (var item in grades)
            {
                Console.WriteLine(item);
            }

        }
    }
}
