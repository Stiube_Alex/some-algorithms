﻿using System;

namespace Matrix_Sum
{
    class Program
    {
        static Random r = new Random();
        static void Main(string[] args)
        {
            int n = 3;//int.Parse(Console.ReadLine());
            int[,] matrixA = new int[n, n];
            int[,] matrixB = new int[n, n];
            generateMatrix(matrixA, n);
            generateMatrix(matrixB, n);
            matrixSum(matrixA, matrixB, n);
        }

        static void generateMatrix(int[,] matrix, int n)
        {
            Console.WriteLine();
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    matrix[i, j] = r.Next(10);
                    Console.Write(matrix[i, j] + " ");
                }
                Console.WriteLine();
            }
        }

        static void matrixSum(int[,] matrixA, int[,] matrixB, int n)
        {
            Console.WriteLine();
            int[,] sumOfMatrix = new int[n,n];
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    sumOfMatrix[i,j] = matrixA[i, j] + matrixB[i, j];
                    Console.Write($"{sumOfMatrix[i,j]} ");
                }
                Console.WriteLine();
            }
        }
    }
}
