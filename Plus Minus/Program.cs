﻿using System;

namespace Plus_Minus
{
    class Program
    {
        static Random r = new Random();
        static void Main(string[] args)
        {
            int n = 6;//int.Parse(Console.ReadLine());
            int[] arr = new int[n];
            generateMatrix(arr);
            for (int i = 0; i < arr.Length; i++)
            {
                Console.Write($"{arr[i]} ");
            }
            Console.WriteLine("\n");
            signAverage(arr);

        }
        static void signAverage(int[] arr)
        {
            float countPoz = 0;
            float countNeg = 0;
            float countZero = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                if (arr[i] > 0)
                {
                    countPoz++;
                }
                if (arr[i] < 0)
                {
                    countNeg++;
                }
                if (arr[i] == 0)
                {
                    countZero++;
                }
            }
            float pozAvg = countPoz / arr.Length;
            float negAvg = countNeg / arr.Length;
            float zeroAvg = countZero / arr.Length;

            Console.WriteLine($"Pozitive: {countPoz}");
            Console.WriteLine($"Negative: {countNeg}");
            Console.WriteLine($"Zero: {countZero}");

            Console.WriteLine("\n");

            Console.WriteLine($"pozAvg: {pozAvg}");
            Console.WriteLine($"negAvg: {negAvg}");
            Console.WriteLine($"zeroAvg: {zeroAvg}");
        }
        static int[] generateMatrix(int[] arr)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = r.Next(-10, 10);
            }
            return arr;
        }
    }
}
