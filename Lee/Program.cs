﻿using System;
using System.IO;
using System.Collections.Generic;

namespace Lee
{
    class Program
    {
        public static int[,] matrix;
        public static int lin, col;

        public static void Load()
        {
            TextReader load = new StreamReader("@../../input.txt");
            List<string> data = new List<string>();
            string buffer = load.ReadLine();
            while (buffer != null)
            {
                data.Add(buffer);
            }
            load.Close();
            lin = data.Count;
            col = data[0].Split().Length;
            matrix = new int[lin, col];
        }
        static void Main(string[] args)
        {
            Load();
            for (int i = 0; i < matrix.Length; i++)
            {
                for (int j = 0; j < matrix.Length; j++)
                {
                    Console.Write(matrix[i, j]);
                }
                Console.WriteLine();
            }
        }
    }
}