﻿using System;

namespace Sales_by_Match
{
    class Program
    {
        static Random r = new Random();
        static void Main(string[] args)
        {
            int n = 9;
            int[] arr = new int[n];
            generate(n, arr);
            print(n, arr);
            ifPair(n, arr);
        }
        static void print(int n, int[] arr)
        {
            for (int i = 0; i < n; i++)
            {
                Console.Write($"{arr[i]} ");
            }
        }
        static int[] generate(int n, int[] arr)
        {
            for (int i = 0; i < n; i++)
            {
                arr[i] = r.Next(1, 4);
            }
            return arr;
        }
        static void ifPair(int n, int[] arr)
        {
            int count = 0;
            int pair = 0;
            int a = 0;

            while (a < 100)
            {
                for (int i = 0; i < n; i++)
                {
                    if (a == arr[i])
                    {
                        count++;
                    }
                    if (count == 2)
                    {
                        pair++;
                        count = 0;
                    }
                }
                count = 0;
                a++;
            }

            Console.WriteLine();
            Console.WriteLine(count);
            Console.WriteLine(pair);
        }
    }
}
