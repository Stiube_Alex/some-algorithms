﻿using System;

namespace Mini_Max_Sum
{
    class Program
    {
        static Random r = new Random();
        static void Main(string[] args)
        {
            int[] arr = new int[5];
            generateArr(arr);
            printArr(arr);
            Console.WriteLine();
            sum(arr);
        }
        static void printArr(int[] arr)
        {
            for (int i = 0; i < 5; i++)
            {
                Console.Write($"{arr[i]} ");
            }
        }
        static int[] generateArr(int[] arr)
        {
            for (int i = 0; i < 5; i++)
            {
                arr[i] = r.Next(-9, 10);
            }
            return arr;
        }
        static void sum(int[] arr)
        {
            Console.WriteLine();
            int[] smallArr = new int[5];
            decimal temp = 0;
            decimal first = 0;
            decimal second = 0;

            for (int i = 0; i < 5; i++)
            {
                smallArr[i] = arr[i];
            }
            int a = 0;
            while (a < 5)
            {
                for (int i = 0; i < 5; i++)
                {
                    smallArr[a] = 0;
                    Console.Write($"{smallArr[i]} ");
                    temp += smallArr[i];
                    second = temp;
                    if (temp > first)
                    {
                        first = temp;
                        if (second < first)
                        {
                            second = first;
                        }
                    }
                }
                for (int i = 0; i < 5; i++)
                {
                    smallArr[i] = arr[i];
                }
                Console.WriteLine();
                temp = 0;
                a++;
            }
            Console.WriteLine();
            Console.WriteLine(first);
            Console.WriteLine(second);
        }
    }
}
