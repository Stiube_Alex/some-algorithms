﻿using System;

namespace Diagonal_Matrix_Difference
{
    class Program
    {
		static int primaryDiagonalSum = 0;
		static int secondaryDiagonalSum = 0;
		static int dif = 0;
		static Random r = new Random();

		static void Main(string[] args)
		{
			int n = int.Parse(Console.ReadLine());
			int[,] matrix = new int[n, n];
			generateAndShow(matrix, n);
			diagonalDif(matrix, n);
			Console.ReadLine();
		}

		private static void generateAndShow(int[,] matrix, int n)
		{
			Console.WriteLine();
			for (int i = 0; i < n; i++)
			{
				for (int j = 0; j < n; j++)
				{
					matrix[i, j] =  r.Next(10);
					Console.Write(matrix[i, j] + " ");
				}
				Console.WriteLine();
			}
		}

		private static void diagonalDif(int[,] matrix, int n)
		{
			Console.WriteLine();
			for (int i = 0; i < n; i++)
			{
				for (int j = 0; j < n; j++)
				{
					if (i == j)
					{
						primaryDiagonalSum += matrix[i, j];
                      
					}
					if (i + j == n - 1)
					{
						secondaryDiagonalSum += matrix[i, j];
					}
				}
			}
			dif = primaryDiagonalSum - secondaryDiagonalSum;

            Console.WriteLine($"Primary diagonal sum = {primaryDiagonalSum}");
            Console.WriteLine($"Secondary diagonal sum = {secondaryDiagonalSum}");
			Console.WriteLine($"Total dif = {dif}");
		}

    }

}
